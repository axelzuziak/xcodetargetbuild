//
//  NSObject_Extension.swift
//
//  Created by Axel Zuziak on 25.01.2016.
//  Copyright © 2016 Axel Zuziak. All rights reserved.
//

import Foundation

extension NSObject {
    class func pluginDidLoad(bundle: NSBundle) {
        let appName = NSBundle.mainBundle().infoDictionary?["CFBundleName"] as? NSString
        if appName == "Xcode" {
        	if sharedPlugin == nil {
        		sharedPlugin = PodBuild(bundle: bundle)
        	}
        }
    }
}