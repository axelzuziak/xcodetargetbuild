//
//  PodBuild.swift
//
//  Created by Axel Zuziak on 25.01.2016.
//  Copyright © 2016 Axel Zuziak. All rights reserved.
//

import AppKit

var sharedPlugin: PodBuild?

class PodBuild: NSObject {

    var bundle: NSBundle
    lazy var center = NSNotificationCenter.defaultCenter()

    init(bundle: NSBundle) {
        self.bundle = bundle

        super.init()
        center.addObserver(self, selector: Selector("createMenuItems"), name: NSApplicationDidFinishLaunchingNotification, object: nil)
    }

    deinit {
        removeObserver()
    }

    func removeObserver() {
        center.removeObserver(self)
    }

    func createMenuItems() {
        removeObserver()

        let item = NSApp.mainMenu!.itemWithTitle("Product")
        if item != nil {
//            let actionMenuItem = NSMenuItem(title:"Do Action", action:"doMenuAction", keyEquivalent:"")
//            actionMenuItem.keyEquivalentModifierMask = Int(NSEventModifierFlags.CommandKeyMask.rawValue) | Int(NSEventModifierFlags.AlternateKeyMask.rawValue)
//            actionMenuItem.keyEquivalent = "b"
//            actionMenuItem.target = self
            item!.submenu!.addItem(NSMenuItem.separatorItem())
            item!.submenu!.addItem(createMenuItem("Run Debug", menuAction: "runDebug"))
            item!.submenu!.addItem(createMenuItem("Run Release", menuAction: "runRelease"))
            item!.submenu!.addItem(createMenuItem("Run Staging", menuAction: "runStaging"))
        }
    }

    func createMenuItem(name: String, menuAction: Selector) -> NSMenuItem {
        let actionMenuItem = NSMenuItem(title: name, action: menuAction, keyEquivalent:"")
        actionMenuItem.target = self
        
        return actionMenuItem
    }
    

    func runDebug() {
        selectScheme("Debug")
        run()
    }
    
    func runRelease() {
        selectScheme("Release")
        run()
    }
    
    func runStaging() {
        selectScheme("Staging")
        run()
    }
    
    func selectScheme(name: String) {
        var str = ""
        let productMenu = NSApp.mainMenu!.itemWithTitle("Product")
        
        let schemeMenu = productMenu?.submenu?.itemWithTitle("Scheme")
        
        for item in (schemeMenu!.submenu!.itemArray) {
            print(item.title)
            if item.title == name {
                schemeMenu!.submenu?.performActionForItemAtIndex((schemeMenu!.submenu?.indexOfItem(item))!)
                return
            }
            
            str += (item.title + " ")
        }
        

        let error = NSError(domain: "No \(name) found" + str, code:42, userInfo:nil)
        NSAlert(error: error).runModal()
        
    }
    
    func run() {
        let productMenu = NSApp.mainMenu!.itemWithTitle("Product")
        let run = productMenu?.submenu?.itemWithTitle("Run")
        productMenu?.submenu?.performActionForItemAtIndex((productMenu?.submenu?.indexOfItem(run!))!)
    }
}

